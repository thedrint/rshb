'use strict';
var $html = $('html'),
	$win = $(window),
	navbar = $('.navbar__nav'),
	MEDIAQUERY = {};
MEDIAQUERY = {
	desktopXL: 1280,
	desktop: 960,
	tablet: 760,
	mobile: 576,
	phone: 480
};
var navbarHandler = function navbarHandler() {
	var navitem = $('.navbar__item'),
		$this = void 0;
	navitem.on('click', function(e) {
		var $this = $(this);
		if (isSmallDevice() && !$this.hasClass('active')) {
			e.preventDefault();
			$this.closest('.navbar__nav').find('.active').removeClass('active');
			$this.addClass('active');
		}
	});
	navitem.on('mouseover', function(e) {
		var $this = $(this);
		$this.closest('.navbar__nav').find('.hover').removeClass('hover');
		$this.addClass('hover'); // if (!$('.navbar-nav__backdrop').is(':visible')) {
		//     $('<div class="navbar-nav__backdrop"></div>').insertAfter(navbar.parents('.navbar'));
		//     $('.navbar-nav__backdrop').fadeIn(150);
		// }
	});
	navitem.on('mouseleave', function(e) {
		if ($(this).hasClass('hover')) {
			$(this).removeClass('hover');
		}
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} // if (!$(this).hasClass('hover')) {
		//     navbar.parents('.navbar').next('.navbar-nav__backdrop').fadeOut(50).remove();
		// }
	});
	navbar.on('mouseleave', function(e) {
		if (!isSmallDevice()) {
			$(this).parents('.navbar').next('.header__backdrop').fadeOut(50).remove();
		}
	});
	$(document).on('click touchstart', '.header__backdrop', function(e) {
		$(this).fadeOut(50).remove();
	});
	$win.on('resize', function() {
		if (!isSmallDevice()) { // $('.active, .hover', navbar).removeClass('active').removeClass('hover');
			if ($('.header__backdrop').is(':visible')) {
				$('.navbar-toggler').trigger('click'); // $('.navbar').next('.header__backdrop').fadeOut(250).remove();
			}
		}
	});
};
var toggleClassOnElement = function toggleClassOnElement() {
	var toggleAttribute = $('*[data-toggle-class]');
	toggleAttribute.each(function() {
		var $this = $(this);
		var toggleClass = $this.attr('data-toggle-class');
		var outsideElement = void 0;
		var toggleElement = void 0;
		typeof $this.attr('data-toggle-target') !== 'undefined' ? toggleElement = $($this.attr('data-toggle-target')) : toggleElement = $this;
		$this.on('click', function(e) {
			if ($this.hasClass('navbar-toggler') || $this.hasClass('navbar__close')) {
				if (!$('.navbar').next('.header__backdrop').length) {
					$('<div class="header__backdrop"></div>').insertAfter($('.navbar'));
					$('.header__backdrop').fadeIn(250);
				} else {
					$('.navbar').next('.header__backdrop').fadeOut(250).remove();
				}
			}
			if ($this.attr('data-toggle-type') !== 'undefined' && $this.attr('data-toggle-type') == 'on') {
				toggleElement.addClass(toggleClass);
			} else if ($this.attr('data-toggle-type') !== 'undefined' && $this.attr('data-toggle-type') == 'off') {
				toggleElement.removeClass(toggleClass);
			} else {
				toggleElement.toggleClass(toggleClass);
			}
			e.preventDefault();
			if ($this.attr('data-toggle-click-outside')) {
				outsideElement = $($this.attr('data-toggle-click-outside'));
				$(document).on('mousedown touchstart', toggleOutside);
			};
		});
		var toggleOutside = function toggleOutside(e) {
			if (outsideElement.has(e.target).length === 0 && !outsideElement.is(e.target) && !toggleAttribute.is(e.target) && toggleElement.hasClass(toggleClass)) {
				toggleElement.removeClass(toggleClass);
				$(document).off('mousedown touchstart', toggleOutside);
			}
		};
	});
};

function isSmallDevice() {
	return $win.width() < MEDIAQUERY.desktop;
}
var promoSlider = {
	slider: $('.js-promo-carousel'),
	sliderSettings: function sliderSettings() {
		return {
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			focusOnSelect: false,
			arrows: false,
			dots: true,
			speed: 500
		};
	},
	init: function init() {
		var width = void 0;
		if (promoSlider.slider.length) {
			width = $('body').width() - 30 >= promoSlider.slider.closest('.wrapper').width() ? promoSlider.slider.closest('.wrapper').width() : $('body').width() - 30;
			if ($(window).width() < 760) {
				width = width + 30;
			}
			promoSlider.slider.parent().width(width);
			promoSlider.slider.slick(promoSlider.sliderSettings());
			$(window).on('resize', function() {
				width = $('body').width() - 30 >= promoSlider.slider.closest('.wrapper').width() ? promoSlider.slider.closest('.wrapper').width() : $('body').width() - 30;
				if ($(window).width() < 760) {
					width = width + 30;
				}
				promoSlider.slider.parent().width(width);
			});
		}
	}
};;
(function($) {
	if (!$.IZ) {
		$.IZ = new Object();
	};
	$.IZ.ImageZoom = function(el, options) {
		var self = this;
		self.$el = $(el);
		self.el = el;
		self.$el.data('IZ.ImageZoom', self);
		self.init = function() {
			if (!$.fn.slick && self.options.useSlick) {
				throw new Error("Slick JS is not installed, but required to use this zoom");
			}
			if (typeof PhotoSwipe === 'undefined') {
				throw new Error('PhotoSwipe is not installed, but required to use this zoom');
			}
			self.options = $.extend({}, $.IZ.ImageZoom.defaultOptions, options);
			self.origImgObjects = self.options.imageObjects != null ? self.options.imageObjects : self.imageObject();
			self.initialSetup();
			if (self.options.useSlick) {
				self.slick();
			}
		};
		self.initialSetup = function() {
			$('body').append(self.pswpHtml());
			self.$trueEl = self.$el;
			self.$trueEl.addClass('image-zoom-hover');
			self.$trueEl.on('click', 'a', function(event) {
				event.preventDefault();
				var currentIndex = self.options.useSlick ? $(this).closest('.slick-slide').data('slick-index') : $(this).parent().index();
				var link = $(this).attr('href');
				if (currentIndex >= 0 && link !== undefined) {
					self.photoSwipe(currentIndex);
				}
			});
		};
		self.photoSwipe = function(index) {
			var options = $.extend({}, self.options.psOptions, {
				index: index
			});
			var pswp = $('.pswp.image-zoom-hover')[0];
			var zoom = new PhotoSwipe(pswp, PhotoSwipeUI_Default, self.origImgObjects, options);
			var realViewportWidth, useLargeImages = false,
				firstResize = true,
				imageSrcWillChange;
			zoom.listen('beforeResize', function() {
				realViewportWidth = zoom.viewportSize.x * window.devicePixelRatio;
				if (useLargeImages && realViewportWidth < 1000) {
					useLargeImages = false;
					imageSrcWillChange = true;
				} else if (!useLargeImages && realViewportWidth >= 1000) {
					useLargeImages = true;
					imageSrcWillChange = true;
				}
				if (imageSrcWillChange && !firstResize) {
					zoom.invalidateCurrItems();
				}
				if (firstResize) {
					firstResize = false;
				}
				imageSrcWillChange = false;
			});
			zoom.listen('gettingData', function(index, item) {
				if (useLargeImages) {
					item.src = item.originalImage.src;
					item.w = item.originalImage.w;
					item.h = item.originalImage.h;
				} else {
					item.src = item.originalImage.src;
					item.w = item.originalImage.w / 2;
					item.h = item.originalImage.h / 2;
				}
			});
			zoom.init();
		};
		self.imageObject = function() {
			var imgArray = [];
			self.$el.children('figure').each(function() {
				var link = $(this).find('a');
				var image = $(this).find('img');
				var sizeLink = link.attr('data-size').split('x');
				var sizeImage = image.attr('data-size').split('x');
				var imageObj = {
					mediumImage: {
						src: image.attr('src'),
						w: parseInt(sizeImage[0], 10),
						h: parseInt(sizeImage[1], 10)
					},
					originalImage: {
						src: link.attr('href'),
						w: parseInt(sizeLink[0], 10),
						h: parseInt(sizeLink[1], 10)
					}
				};
				if (link.attr('href') !== undefined) {
					imgArray.push(imageObj);
				}
			});
			return imgArray;
		};
		self.slick = function() {
			var slickOptions = self.options.slickOptions;
			var slick = self.$trueEl.slick(slickOptions);
			return slick;
		};
		self.pswpHtml = function() {
			var pswpHtml = '<div class="pswp image-zoom-hover" tabindex="-1" role="dialog" aria-hidden="true"> \
                                <div class="pswp__bg"></div> \
                                <div class="pswp__scroll-wrap"> \
                                    <div class="pswp__container"> \
                                        <div class="pswp__item"></div> \
                                        <div class="pswp__item"></div> \
                                        <div class="pswp__item"></div> \
                                    </div> \
                                    <div class="pswp__ui pswp__ui--hidden"> \
                                        <div class="pswp__top-bar"> \
                                            <div class="pswp__counter"></div> \
                                            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button> \
                                            <button class="pswp__button pswp__button--share" title="Share"></button> \
                                            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> \
                                            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button> \
                                            <div class="pswp__preloader"> \
                                                <div class="pswp__preloader__icn"> \
                                                    <div class="pswp__preloader__cut"> \
                                                        <div class="pswp__preloader__donut"></div> \
                                                    </div> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"> \
                                            <div class="pswp__share-tooltip"></div> \
                                        </div> \
                                        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> \
                                        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button> \
                                        <div class="pswp__caption"> \
                                            <div class="pswp__caption__center"></div> \
                                        </div> \
                                    </div> \
                                </div> \
                            </div>';
			if (self.options.pswpHtml !== '') {
				pswpHtml = self.options.pswpHtml;
			}
			return pswpHtml;
		};
		self.init();
	};
	$.IZ.ImageZoom.defaultOptions = {
		pswpHtml: '',
		psOptions: {
			bgOpacity: 1,
			showHideOpacity: true,
			clickToCloseNonZoomable: false,
			barsSize: {
				top: 0,
				bottom: 0
			},
			shareEl: false,
			fullscreenEl: false,
			closeOnScroll: false,
			closeEl: true,
			captionEl: false,
			tapToToggleControls: false,
			history: false,
			zoomEl: false,
			pinchToClose: false,
			loop: false,
			counterEl: false,
			arrowEl: false,
			scaleMode: "orig"
		},
		slickOptions: {
			dots: true,
			arrows: false,
			infinite: true,
			speed: 500,
			fade: true,
			focusOnSelect: false,
			lazyLoad: "ondemand",
			cssEase: "linear",
			adaptiveHeight: true,
			mobileFirst: true,
			centerMode: true,
			responsive: [{
				breakpoint: 767,
				settings: {
					arrows: false,
					dots: true,
					customPaging: function customPaging(slider, i) {
						var item = $(slider.$slides[i]).find("img");
						return '<button class="product-zoom__tab"><img class="img-thumbnail" src="' + item.attr("data-thumb") + '" alt="' + item.attr("data-alt-thumb") + '"></button>';
					}
				}
			}]
		},
		imageObjects: null,
		useSlick: true
	};
	$.fn.iz_hover = function(options) {
		return this.each(function() {
			new $.IZ.ImageZoom(this, options);
		});
	};
})(jQuery);
$(function() {
	svg4everybody();
	navbarHandler();
	toggleClassOnElement();
	if ($('.js-promo-carousel').length) {
		promoSlider.init();
	}
	if ($('.js-cards-gallery').length) {
		$('.js-cards-gallery').iz_hover({
			psOptions: {
				bgOpacity: 0.9,
				barsSize: {
					top: 0,
					bottom: 0
				},
				showHideOpacity: true,
				shareEl: false,
				fullscreenEl: false,
				closeOnScroll: false,
				clickToCloseNonZoomable: false,
				closeEl: true,
				captionEl: false,
				tapToToggleControls: false,
				history: false,
				zoomEl: false,
				counterEl: false
			},
			imageObjects: null,
			useSlick: false
		});
	} // TODO: for testing
	$('.js-office-link').unbind();
	$('.js-office-link').on('click', function(e) {
		var $this = $(this);
		e.preventDefault();
		if ($this.closest('.b-office').is('.is-open')) {
			$this.addClass('is-open');
			$this.closest('.b-office').find('.b-office-full').slideUp(function() {
				$this.closest('.b-office').removeClass('is-open');
			});
		} else {
			$this.removeClass('is-open');
			$this.closest('.b-office').find('.b-office-full').slideDown(function() {
				$this.closest('.b-office').addClass('is-open');
			});
		}
	});
});

//# sourceMappingURL=main.js.map
